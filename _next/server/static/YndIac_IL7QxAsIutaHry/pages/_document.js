module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 16);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("styled-components");

/***/ }),
/* 2 */,
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

var envConfig = __webpack_require__(5);

module.exports = {
  translation: {
    // default / fallback language
    defaultLanguage: 'en',
    localesPath: "http://localhost:8000/locales/",
    // needed for serverside preload
    allLanguages: ['en', 'pt', 'es'],
    // optional settings needed for subpath (/de/page1) handling
    enableSubpaths: true,
    subpathsOnNonDefaultLanguageOnly: false // only redirect to /lng/ if not default language

  }
};

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

var i18next = __webpack_require__(10);

var XHR = __webpack_require__(11);

var LanguageDetector = __webpack_require__(12);

var envConfig = __webpack_require__(5);

var config = __webpack_require__(3);

var i18n = i18next.default ? i18next.default : i18next;
var options = {
  fallbackLng: config.translation.defaultLanguage,
  load: 'languageOnly',
  // we only provide en, de -> no region specific locals like en-US, de-DE
  // have a common namespace used around the full app
  ns: ['common'],
  defaultNS: 'common',
  debug: false,
  // process.env.NODE_ENV !== 'production',
  saveMissing: true,
  interpolation: {
    escapeValue: false,
    // not needed for react!!
    formatSeparator: ',',
    format: function format(value, _format) {
      if (_format === 'uppercase') return value.toUpperCase();
      return value;
    }
  },
  detection: {
    caches: ['localStorage', 'cookie']
  },
  // we load the from the static folder which is avaiable on npm run export
  backend: {
    loadPath: "http://localhost:8000/static/locales/{{lng}}/{{ns}}.json",
    addPath: "http://localhost:8000/static/locales/{{lng}}/{{ns}}.missing.json"
  }
}; // for browser use xhr backend to load translations and browser lng detector

if (false) {} // initialize if not already initialized


if (!i18n.isInitialized) i18n.init(options); // a simple helper to getInitialProps passed on loaded i18n data

i18n.getInitialProps = function (req, namespaces) {
  if (!namespaces) namespaces = i18n.options.defaultNS;
  if (typeof namespaces === 'string') namespaces = [namespaces]; // do not serialize i18next instance avoid sending it to client

  if (req && req.i18n) req.i18n.toJSON = function () {
    return null;
  };
  var ret = {
    i18n: req ? req.i18n : i18n // use the instance on req - fixed language on request (avoid issues in race conditions with lngs of different users)

  }; // for serverside pass down initial translations

  if (req && req.i18n) {
    var initialI18nStore = {};
    req.i18n.languages.forEach(function (l) {
      initialI18nStore[l] = {};
      namespaces.forEach(function (ns) {
        initialI18nStore[l][ns] = (req.i18n.services.resourceStore.data[l] || {})[ns] || {};
      });
    });
    ret.initialI18nStore = initialI18nStore;
    ret.initialLanguage = req.i18n.language;
  }

  return ret;
};

module.exports = i18n;

/***/ }),
/* 5 */
/***/ (function(module, exports) {

var assetsPrefix = 'http://localhost:8000';
var assetsPrefixSandbox = 'https://github.com/uphold/website-hbc/tree/sandbox/static';
var isSandbox = process.env.RUNNING_ENV !== 'production';
module.exports = {
  'process.env.COOKIE_KEY': !isSandbox ? '_token' : '_token_com_k8s_sandbox',
  'process.env.URL': !isSandbox ? 'https://uphold.com' : 'https://sandbox.uphold.com',
  'process.env.HBC_URL': !isSandbox ? 'https://sandbox.hedgedbitcoin.com/app' : 'https://hedgedbitcoin.com/app',
  'process.env.API_URL': !isSandbox ? 'https://api.uphold.com/v0/' : 'http://api-sandbox.uphold.com/v0/',
  'process.env.ASSETS_URL': process.env.RUNNING_ENV === 'production' || !isSandbox ? isSandbox ? assetsPrefixSandbox : assetsPrefix : './static'
};

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = require("react-i18next");

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("next/document");

/***/ }),
/* 8 */,
/* 9 */,
/* 10 */
/***/ (function(module, exports) {

module.exports = require("i18next");

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = require("i18next-xhr-backend");

/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports = require("i18next-browser-languagedetector");

/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),
/* 14 */,
/* 15 */,
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(26);


/***/ }),
/* 17 */,
/* 18 */,
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */,
/* 23 */,
/* 24 */,
/* 25 */,
/* 26 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(0);
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: external "next/document"
var document_ = __webpack_require__(7);
var document_default = /*#__PURE__*/__webpack_require__.n(document_);

// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(13);
var router_default = /*#__PURE__*/__webpack_require__.n(router_);

// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(1);

// EXTERNAL MODULE: external "react-i18next"
var external_react_i18next_ = __webpack_require__(6);

// EXTERNAL MODULE: ./i18n.js
var i18n = __webpack_require__(4);
var i18n_default = /*#__PURE__*/__webpack_require__.n(i18n);

// EXTERNAL MODULE: ./config.js
var config = __webpack_require__(3);
var config_default = /*#__PURE__*/__webpack_require__.n(config);

// CONCATENATED MODULE: ./lib/languagePathCorrection.js


var _config$translation = config_default.a.translation,
    defaultLanguage = _config$translation.defaultLanguage,
    allLanguages = _config$translation.allLanguages,
    subpathsOnNonDefaultLanguageOnly = _config$translation.subpathsOnNonDefaultLanguageOnly;
/* harmony default export */ var languagePathCorrection = (function (currentRoute) {
  var currentLanguage = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : i18n_default.a.languages[0];

  if (!allLanguages.includes(currentLanguage)) {
    return currentRoute;
  }

  var correctRoute = currentRoute;
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = allLanguages[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var lng = _step.value;

      if (currentRoute.startsWith("/".concat(lng, "/"))) {
        correctRoute = correctRoute.replace("/".concat(lng, "/"), '/');
        break;
      }
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return != null) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  if (!subpathsOnNonDefaultLanguageOnly || currentLanguage !== defaultLanguage) {
    correctRoute = "/".concat(currentLanguage).concat(correctRoute);
  }

  return correctRoute;
});
// CONCATENATED MODULE: ./pages/_document.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _document_MyDocument; });
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }









var enableSubpaths = config["translation"].enableSubpaths;

if (enableSubpaths) {
  router_default.a.events.on('routeChangeStart', function (originalRoute) {
    var correctedPath = languagePathCorrection(originalRoute);

    if (correctedPath !== originalRoute) {
      router_default.a.replace(correctedPath, correctedPath, {
        shallow: true
      });
    }
  });
  i18n_default.a.on('languageChanged', function (lng) {
    if (false) { var correctedPath, originalRoute; }
  });
}

var _document_MyDocument =
/*#__PURE__*/
function (_Document) {
  _inherits(MyDocument, _Document);

  function MyDocument() {
    _classCallCheck(this, MyDocument);

    return _possibleConstructorReturn(this, _getPrototypeOf(MyDocument).apply(this, arguments));
  }

  _createClass(MyDocument, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          pageProps = _this$props.pageProps,
          styleTags = _this$props.styleTags,
          initialLanguage = _this$props.initialLanguage;
      return external_react_default.a.createElement("html", null, external_react_default.a.createElement(external_react_i18next_["NamespacesConsumer"], _extends({}, pageProps, {
        ns: ['common'],
        i18n: pageProps && pageProps.i18n || i18n_default.a,
        wait: false,
        initialLanguage: initialLanguage
      }), function () {
        return external_react_default.a.createElement(external_react_default.a.Fragment, null, external_react_default.a.createElement(document_["Head"], null, external_react_default.a.createElement("meta", {
          name: "viewport",
          content: "width=device-width, initial-scale=1"
        }), styleTags), external_react_default.a.createElement("body", null, external_react_default.a.createElement(document_["Main"], null), external_react_default.a.createElement(document_["NextScript"], null)));
      }));
    }
  }], [{
    key: "getInitialProps",
    value: function getInitialProps(_ref) {
      var renderPage = _ref.renderPage,
          req = _ref.req;
      var sheet = new external_styled_components_["ServerStyleSheet"]();
      var page = renderPage(function (App) {
        return function (props) {
          return sheet.collectStyles(external_react_default.a.createElement(App, props));
        };
      });
      var styleTags = sheet.getStyleElement();
      return _objectSpread({}, page, {
        styleTags: styleTags,
        initialLanguage: req.language ? req.language : config["translation"].defaultLanguage
      });
    }
  }]);

  return MyDocument;
}(document_default.a);



/***/ })
/******/ ]);